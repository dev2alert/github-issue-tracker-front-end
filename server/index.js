import console from "console";
import "dotenv/config";
import express from "express";
import {join} from "path";
import process from "process";

export const app = express();
export const appPath = process.cwd();
export const distPath = join(appPath, "./dist");
export const pagePath = join(distPath, "./index.html");

export const serverPort = Number(process.env.PORT ?? 80);

app.use(express.static(distPath));
app.use((req, res) => res.sendFile(pagePath));
app.listen(serverPort, () => {
    console.log("Github Issue Tracker Frontend listening on port " + serverPort);
});
