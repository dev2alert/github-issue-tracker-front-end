import react from "@vitejs/plugin-react";
import {config} from "dotenv";
import {join} from "path";
import {defineConfig} from "vite";
import {createHtmlPlugin} from "vite-plugin-html";
import {VitePWA} from "vite-plugin-pwa";

export default defineConfig(({mode}) => {
    const devMode: boolean = mode === "development";

    config({
        path: join(process.cwd(), "./.env." + mode)
    });

    return {
        server: {
            host: "0.0.0.0"
        },
        build: {
            sourcemap: devMode,
            rollupOptions: {
                onwarn(warning, warn) {
                    if (warning.code === "MODULE_LEVEL_DIRECTIVE") {
                        return;
                    }

                    warn(warning);
                }
            }
        },
        plugins: [
            createHtmlPlugin({
                minify: true
            }),
            react(),
            VitePWA({
                registerType: "autoUpdate",
                injectRegister: "script",
                devOptions: {
                    enabled: true
                },
                manifest: {
                    name: "Github Issue Tracker",
                    short_name: "Issue Tracker",
                    description: "Search issues",
                    theme_color: "#1f2328",
                    icons: [
                        {
                            src: "/icon-48x48.png",
                            sizes: "48x48",
                            type: "image/png"
                        },
                        {
                            src: "/icon-72x72.png",
                            sizes: "72x72",
                            type: "image/png"
                        },
                        {
                            src: "/icon-96x96.png",
                            sizes: "96x96",
                            type: "image/png"
                        },
                        {
                            src: "/icon-128x128.png",
                            sizes: "128x128",
                            type: "image/png"
                        },
                        {
                            src: "/icon-144x144.png",
                            sizes: "144x144",
                            type: "image/png"
                        },
                        {
                            src: "/icon-152x152.png",
                            sizes: "152x152",
                            type: "image/png"
                        },
                        {
                            src: "/icon-192x192.png",
                            sizes: "192x192",
                            type: "image/png"
                        },
                        {
                            src: "/icon-512x512.png",
                            sizes: "512x512",
                            type: "image/png"
                        }
                    ]
                }
            })
        ],
        resolve: {
            alias: {
                "@common": join(__dirname, "./src/common"),
                "@pages": join(__dirname, "./src/pages")
            }
        }
    };
});
