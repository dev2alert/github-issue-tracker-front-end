import React, {useContext, useMemo} from "react";
import {QueryClient, QueryClientProvider} from "react-query";
import axios, {AxiosInstance} from "axios";
import {apiUrl} from "@common/config";

export type Api = AxiosInstance;

export const ApiContext = React.createContext<Api | null>(null);

export const ApiProvider: React.FC<React.PropsWithChildren> = ({children}) => {
    const api = useMemo<Api>(
        () =>
            axios.create({
                baseURL: apiUrl
            }),
        []
    );

    const queryClient = useMemo(
        () =>
            new QueryClient({
                defaultOptions: {
                    queries: {
                        refetchOnWindowFocus: false
                    }
                }
            }),
        []
    );

    return (
        <ApiContext.Provider value={api}>
            <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
        </ApiContext.Provider>
    );
};

export function useApi(): Api {
    const api = useContext(ApiContext);

    if (!api) {
        throw new Error("useApi must be used within a ApiProvider.");
    }

    return api;
}

export * from "./hooks";
export * from "./types";
