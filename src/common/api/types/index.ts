export interface IssueUser {
    id: number;
    login: string;
    node_id: string;
    avatar_url: string;
    gravatar_id: string;
    url: string;
    html_url: string;
    followers_url: string;
    following_url: string;
    gists_url: string;
    starred_url: string;
    subscriptions_url: string;
    organizations_url: string;
    repos_url: string;
    events_url: string;
    received_events_url: string;
    type: string;
    site_admin: boolean;
}

export interface IssueLabel {
    id: number;
    node_id: string;
    url: string;
    name: string;
    default: boolean;
    description: string | null;
    color: string;
}

export interface Issue {
    id: number;
    url: string;
    repository_url: string;
    labels_url: string;
    comments_url: string;
    events_url: string;
    html_url: string;
    node_id: string;
    number: string;
    title: string;
    user: IssueUser;
    labels: IssueLabel[];
    state: string;
    locked: boolean;
    assignee: null;
    assignees: unknown[];
    milestone: null;
    comments: number;
    created_at: string;
    updated_at: string;
    closed_at: null;
    author_association: string;
    active_lock_reason: null;
    body: string;
    reactions: unknown;
    timeline_url: string;
    performed_via_github_app: null;
    state_reason: null;
}

export interface IssuesErrorResult {
    result: "error";
    message: string;
    status: number | null;
}

export interface IssuesSuccessResult {
    result: "success";
    total_count: number;
    incomplete_results: boolean;
    items: Issue[];
}

export type IssuesResult = IssuesSuccessResult | IssuesErrorResult;

export interface IssueErrorResult {
    result: "error";
    message: string;
    status: number | null;
}

export interface IssueSuccessResult extends Issue {
    result: "success";
}

export type IssueResult = IssueSuccessResult | IssueErrorResult;

export interface Log {
    _id: string;
    type: string;
    userIp: string;
    time: string;
}

export interface LogsResult {
    result: "success";
    items: Log[];
    page: number;
    perPage: number;
    total: number;
    totalPages: number;
}
