import {UseQueryResult, useQuery} from "react-query";
import {IssueResult, useApi} from "..";

export interface UseIssueQueryOptions {
    owner: string;
    repo: string;
    number: number;
}

export type UseIssueQueryResult = UseQueryResult<IssueResult>;

export function useIssueQuery({owner, repo, number}: UseIssueQueryOptions): UseIssueQueryResult {
    const api = useApi();

    return useQuery(["issue", {owner, repo, number}], async () => {
        const {data: result} = await api.get("/issues/" + number, {
            params: {
                owner,
                repo
            }
        });

        return result;
    });
}
