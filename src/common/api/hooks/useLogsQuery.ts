import {UseQueryResult, useQuery} from "react-query";
import {LogsResult, useApi} from "..";

export interface UseLogsQueryOptions {
    page?: number;
    perPage?: number;
}

export type UseLogsQueryResult = UseQueryResult<LogsResult>;

export function useLogsQuery({page, perPage}: UseLogsQueryOptions = {}): UseLogsQueryResult {
    const api = useApi();

    return useQuery(
        ["logs", {page, perPage}],
        async () => {
            const {data: result} = await api.get("/logs", {params: {page, perPage}});

            return result;
        },
        {
            keepPreviousData: true
        }
    );
}
