import {UseQueryResult, useQuery} from "react-query";
import {IssuesResult, useApi} from "..";

export interface UseIssuesQueryOptions {
    owner: string;
    repo: string;
    page?: number;
    perPage?: number;
}

export type UseIssuesQueryResult = UseQueryResult<IssuesResult>;

export function useIssuesQuery({
    owner,
    repo,
    page,
    perPage
}: UseIssuesQueryOptions): UseIssuesQueryResult {
    const api = useApi();

    return useQuery(
        ["issues", {owner, repo, page, perPage}],
        async () => {
            const {data: result} = await api.get<IssuesResult>("/issues", {
                params: {owner, repo, page, perPage}
            });

            return result;
        },
        {
            keepPreviousData: true
        }
    );
}
