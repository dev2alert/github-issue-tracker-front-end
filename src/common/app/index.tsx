import React, {useMemo} from "react";
import {RouterProvider, createBrowserRouter} from "react-router-dom";
import {ThemeProvider, createTheme} from "@mui/material/styles";
import {LoadingPage} from "@pages/Loading";
import {NotFoundPage} from "@pages/NotFound";
import {ApiProvider} from "@common/api";
import {AppGlobalStyle} from "./GlobalStyle";
import {AppPageLayout} from "./PageLayout";

export const TrackerPage = React.lazy(() => import("@pages/Tracker"));

export const IssuePage = React.lazy(() => import("@pages/Issue"));

export const StatsPage = React.lazy(() => import("@pages/Stats"));

export function App(): React.ReactElement {
    const theme = useMemo(
        () =>
            createTheme({
                palette: {
                    primary: {
                        main: "#1f2328"
                    }
                }
            }),
        []
    );

    const router = useMemo(
        () =>
            createBrowserRouter([
                {
                    path: "/",
                    element: <AppPageLayout />,
                    children: [
                        {
                            index: true,
                            element: (
                                <React.Suspense fallback={<LoadingPage />}>
                                    <TrackerPage />
                                </React.Suspense>
                            )
                        },
                        {
                            path: ":owner/:repo/issues/:number",
                            element: (
                                <React.Suspense fallback={<LoadingPage />}>
                                    <IssuePage />
                                </React.Suspense>
                            )
                        },
                        {
                            path: "stats",
                            element: (
                                <React.Suspense fallback={<LoadingPage />}>
                                    <StatsPage />
                                </React.Suspense>
                            )
                        },
                        {
                            path: "*",
                            element: <NotFoundPage />
                        }
                    ]
                }
            ]),
        []
    );

    return (
        <ThemeProvider theme={theme}>
            <ApiProvider>
                <React.Suspense fallback={<LoadingPage />}>
                    <RouterProvider router={router} />
                </React.Suspense>
            </ApiProvider>
            <AppGlobalStyle />
        </ThemeProvider>
    );
}
