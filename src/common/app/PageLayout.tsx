import React from "react";
import {Outlet} from "react-router-dom";
import {AppHeader} from "./Header";

export const AppPageLayout: React.FC = () => {
    return (
        <>
            <AppHeader />
            <Outlet />
        </>
    );
};
