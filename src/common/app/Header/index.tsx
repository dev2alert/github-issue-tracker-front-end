import React from "react";
import {Link} from "react-router-dom";
import GitHubIcon from "@mui/icons-material/GitHub";
import {Box, Typography} from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Container from "@mui/material/Container";
import Toolbar from "@mui/material/Toolbar";

export const AppHeader: React.FC = () => {
    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters sx={{display: "flex", justifyContent: "center"}}>
                    <Box
                        component={Link}
                        to="/"
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            color: "white",
                            textDecoration: "none"
                        }}
                    >
                        <GitHubIcon sx={{mr: 2}} />
                        <Typography
                            variant="h6"
                            noWrap
                            sx={{
                                mr: 2,
                                display: "flex",
                                fontFamily: "monospace",
                                fontWeight: 700,
                                fontSize: 18,
                                color: "inherit",
                                textDecoration: "none"
                            }}
                        >
                            Github Issue Tracker
                        </Typography>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
