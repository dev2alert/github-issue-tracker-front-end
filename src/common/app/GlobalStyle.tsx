import {createGlobalStyle} from "styled-components";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import "normalize.css";

export const AppGlobalStyle = createGlobalStyle`
    body {
        background: #fafafa;
    }
`;
