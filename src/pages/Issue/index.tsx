import React, {useMemo} from "react";
import {useParams} from "react-router-dom";
import {TinyColor} from "@ctrl/tinycolor";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Chip from "@mui/material/Chip";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import LoadingPage from "@pages/Loading";
import NotFoundPage from "@pages/NotFound";
import {MuiMarkdown, getOverrides} from "mui-markdown";
import {Issue, UseIssueQueryOptions, useIssueQuery} from "@common/api";

export const IssuePage: React.FC = () => {
    const params = useParams();

    const issueQueryOptions = useMemo<UseIssueQueryOptions>(() => {
        const owner: string = params.owner ?? "facebook";
        const repo: string = params.repo ?? "react";
        const number = Number(params.number ?? 1);

        return {owner, repo, number};
    }, [params]);
    const issueQuery = useIssueQuery(issueQueryOptions);

    if (issueQuery.isFetching) {
        return <LoadingPage />;
    }
    if (!issueQuery.isSuccess || issueQuery.data.result === "error") {
        return <NotFoundPage />;
    }

    const issue: Issue = issueQuery.data;

    return (
        <Container sx={{mt: 4}}>
            <Box component={Paper} sx={{padding: 2, mb: 2}}>
                <Box
                    sx={{
                        display: "flex",
                        flexWrap: "wrap",
                        gap: 1
                    }}
                >
                    <Typography variant="h5" color="#656d76">
                        #{issue.number}
                    </Typography>
                    <Typography variant="h5" component="h1">
                        {issue.title}
                    </Typography>
                </Box>
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "flex-start",
                        gap: 1,
                        mt: 2
                    }}
                >
                    <Typography
                        sx={{
                            fontWeight: 500
                        }}
                    >
                        Author
                    </Typography>
                    <Box
                        component="a"
                        href={issue.user.html_url}
                        target="_blank"
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: 2,
                            textDecoration: "none",
                            color: "black"
                        }}
                    >
                        <Avatar src={issue.user.avatar_url} alt={issue.user.login} />
                        <Typography sx={{fontSize: 14, fontWeight: "500"}}>
                            {issue.user.login}
                        </Typography>
                    </Box>
                </Box>
                {issue.labels.length !== 0 ? (
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-start",
                            gap: 1,
                            mt: 2
                        }}
                    >
                        <Typography
                            sx={{
                                fontWeight: 500
                            }}
                        >
                            Labels
                        </Typography>
                        <Stack direction="row" gap={1} flexWrap="wrap">
                            {issue.labels.map((label) => (
                                <Chip
                                    key={label.id}
                                    label={label.name}
                                    size="small"
                                    sx={{
                                        color:
                                            new TinyColor(label.color).getLuminance() > 0.179
                                                ? "#000"
                                                : "#fff",
                                        background: "#" + label.color
                                    }}
                                />
                            ))}
                        </Stack>
                    </Box>
                ) : null}
                <Box sx={{display: "flex", justifyContent: "flex-end", mt: "-36px"}}>
                    <Button
                        component="a"
                        href={issue.html_url}
                        target="_blank"
                        variant="text"
                        endIcon={<ArrowForwardIcon />}
                    >
                        Github Page
                    </Button>
                </Box>
            </Box>
            <Box
                component={Paper}
                sx={{
                    mt: 4,
                    padding: 2,
                    mb: 2
                }}
            >
                <MuiMarkdown
                    overrides={{
                        ...getOverrides(),
                        h3: {
                            component: Typography,
                            props: {
                                component: "h3",
                                sx: {
                                    fontWeight: "500",
                                    fontSize: "1.25em",
                                    lineHeight: "1.25",
                                    marginTop: "16px",
                                    marginBottom: "16px"
                                }
                            }
                        },
                        p: {
                            component: Typography,
                            props: {
                                sx: {
                                    fontSize: "14px",
                                    lineHeight: "1.25",
                                    marginBottom: "16px"
                                }
                            }
                        }
                    }}
                >
                    {issue.body}
                </MuiMarkdown>
            </Box>
        </Container>
    );
};

export default IssuePage;
