import React, {useMemo} from "react";
import {Link} from "react-router-dom";
import {TinyColor} from "@ctrl/tinycolor";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Chip from "@mui/material/Chip";
import Paper from "@mui/material/Paper";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import {useTracker} from "../Context";

export const TrackerIssueTable: React.FC = () => {
    const {
        issuesQuery,
        issuesQueryOptions: {owner, repo, perPage}
    } = useTracker();

    const loadingNode = useMemo<React.ReactNode>(
        () => (
            <TableBody>
                {[...Array(perPage ?? 10)].map((_, index) => (
                    <TableRow key={index}>
                        <TableCell width="10%">
                            <Skeleton width={60} />
                        </TableCell>
                        <TableCell width="100%">
                            <Skeleton />
                        </TableCell>
                        <TableCell width="50%">
                            <Stack direction="row" gap={1}>
                                <Chip label={<Skeleton width={70} />} size="small" />
                                <Chip label={<Skeleton width={70} />} size="small" />
                            </Stack>
                        </TableCell>
                        <TableCell width="25%">
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: 2,
                                    textDecoration: "none",
                                    color: "black"
                                }}
                            >
                                <Skeleton variant="circular" width={40} height={40} />
                                <Skeleton width={120} />
                            </Box>
                        </TableCell>
                        <TableCell>
                            <Button variant="text" endIcon={<ArrowForwardIcon />} disabled>
                                Details
                            </Button>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        ),
        []
    );

    if (issuesQuery.isSuccess && issuesQuery.data.result === "error") {
        let error: string;
        switch (issuesQuery.data.status) {
            case 404:
                error = "Issues not found";
                break;
            default:
                error = issuesQuery.data.message;
                break;
        }

        return (
            <Box sx={{mt: 4.5, mb: 4}}>
                <Typography align="center">{error}</Typography>
            </Box>
        );
    }
    if (
        issuesQuery.isSuccess &&
        issuesQuery.data.result === "success" &&
        issuesQuery.data.items.length === 0
    ) {
        return (
            <Box sx={{mt: 4.5, mb: 4}}>
                <Typography align="center">Issue list empty</Typography>
            </Box>
        );
    }

    return (
        <TableContainer component={Paper} sx={{mt: 2}}>
            <Table sx={{minWidth: 650}}>
                <TableHead>
                    <TableRow>
                        <TableCell>№</TableCell>
                        <TableCell>Title</TableCell>
                        <TableCell>Labels</TableCell>
                        <TableCell>Author</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                {issuesQuery.isSuccess &&
                !issuesQuery.isFetching &&
                issuesQuery.data.result === "success" ? (
                    <TableBody>
                        {issuesQuery.data.items.map((issue) => (
                            <TableRow key={issue.id}>
                                <TableCell width="10%">#{issue.number}</TableCell>
                                <TableCell width="100%">
                                    <Typography
                                        component={Link}
                                        to={"/" + owner + "/" + repo + "/issues/" + issue.number}
                                        sx={{
                                            fontSize: 14,
                                            color: "black",
                                            textDecoration: "none"
                                        }}
                                    >
                                        {issue.title}
                                    </Typography>
                                </TableCell>
                                <TableCell width="50%">
                                    <Stack direction="row" gap={1} flexWrap="wrap">
                                        {issue.labels.map((label) => (
                                            <Chip
                                                key={label.id}
                                                label={label.name}
                                                size="small"
                                                sx={{
                                                    color:
                                                        new TinyColor(label.color).getLuminance() >
                                                        0.179
                                                            ? "#000"
                                                            : "#fff",
                                                    background: "#" + label.color
                                                }}
                                            />
                                        ))}
                                    </Stack>
                                </TableCell>
                                <TableCell width="25%">
                                    <Box
                                        component="a"
                                        href={issue.user.html_url}
                                        target="_blank"
                                        sx={{
                                            display: "flex",
                                            alignItems: "center",
                                            gap: 2,
                                            textDecoration: "none",
                                            color: "black"
                                        }}
                                    >
                                        <Avatar
                                            src={issue.user.avatar_url}
                                            alt={issue.user.login}
                                        />
                                        <Typography sx={{fontSize: 14, fontWeight: "500"}}>
                                            {issue.user.login}
                                        </Typography>
                                    </Box>
                                </TableCell>
                                <TableCell>
                                    <Button
                                        component={Link}
                                        to={"/" + owner + "/" + repo + "/issues/" + issue.number}
                                        variant="text"
                                        endIcon={<ArrowForwardIcon />}
                                    >
                                        Details
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                ) : null}
                {issuesQuery.isFetching ? loadingNode : null}
            </Table>
        </TableContainer>
    );
};
