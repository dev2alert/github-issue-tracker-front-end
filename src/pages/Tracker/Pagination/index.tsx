import React, {useCallback, useMemo} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import Pagination from "@mui/material/Pagination";
import Paper from "@mui/material/Paper";
import {useTracker} from "../Context";

export const TrackerPagination: React.FC = () => {
    const navigate = useNavigate();
    const {search} = useLocation();
    const {
        issuesQuery,
        issuesQueryOptions: {page = 1, perPage = 30}
    } = useTracker();

    const count = useMemo<number>(() => {
        if (issuesQuery.isSuccess && issuesQuery.data.result === "success") {
            return Math.ceil(issuesQuery.data.total_count / perPage);
        } else {
            return 0;
        }
    }, [issuesQuery.isSuccess, issuesQuery.data, perPage]);

    const handleChange = useCallback(
        (event: React.ChangeEvent<unknown>, page: number) => {
            const params = new URLSearchParams(search);

            if (page !== 1) {
                params.set("page", page.toString());
            } else {
                params.delete("page");
            }

            navigate("/?" + params);
        },
        [search]
    );

    if (!issuesQuery.isSuccess || issuesQuery.data.result === "error" || count <= 1) {
        return null;
    }

    return (
        <Paper sx={{display: "flex", justifyContent: "center", mt: 2, mb: 2, padding: 2}}>
            <Pagination page={page} count={count} color="primary" onChange={handleChange} />
        </Paper>
    );
};
