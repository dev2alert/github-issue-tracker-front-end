import React, {useContext} from "react";
import {UseIssuesQueryOptions, UseIssuesQueryResult} from "@common/api";

export interface TrackerContextValue {
    issuesQuery: UseIssuesQueryResult;
    issuesQueryOptions: UseIssuesQueryOptions;
}

export const TrackerContext = React.createContext<TrackerContextValue | null>(null);

export const TrackerProvider: React.FC<TrackerContextValue & React.PropsWithChildren> = ({
    children,
    ...value
}) => {
    return <TrackerContext.Provider value={value}>{children}</TrackerContext.Provider>;
};

export function useTracker(): TrackerContextValue {
    const value = useContext(TrackerContext);

    if (!value) {
        throw new Error("useTracker must be used within a TrackerProvider.");
    }

    return value;
}
