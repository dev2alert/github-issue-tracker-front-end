import React, {useCallback, useMemo} from "react";
import {useNavigate} from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import {Field, Formik, FormikHelpers} from "formik";
import {TextField} from "formik-mui";
import {ObjectSchema, object, string} from "yup";
import {useTracker} from "../Context";

export interface TrackerSearchFormValues {
    owner: string;
    repo: string;
}

export const TrackerSearch: React.FC = () => {
    const {
        issuesQueryOptions: {owner, repo}
    } = useTracker();
    const navigate = useNavigate();

    const validationSchema = useMemo<ObjectSchema<TrackerSearchFormValues>>(
        () =>
            object().shape({
                owner: string().required(""),
                repo: string().required("")
            }),
        []
    );

    const handleSubmit = useCallback(
        (
            {owner, repo}: TrackerSearchFormValues,
            {setSubmitting}: FormikHelpers<TrackerSearchFormValues>
        ) => {
            const params = new URLSearchParams();

            params.set("owner", owner);
            params.set("repo", repo);

            navigate("/?" + params);
            setSubmitting(false);
        },
        []
    );

    return (
        <Formik<TrackerSearchFormValues>
            initialValues={{
                owner,
                repo
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
        >
            {({handleSubmit}) => (
                <Paper sx={{padding: 2}}>
                    <Box
                        sx={{
                            display: "flex",
                            gap: 2,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <Box>
                            <Typography variant="h5" component="h2">
                                Search issues
                            </Typography>
                            <Box
                                component="form"
                                sx={{
                                    display: "flex",
                                    gap: 2,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    mt: 2
                                }}
                                onSubmit={handleSubmit}
                            >
                                <Field
                                    component={TextField}
                                    name="owner"
                                    label="Owner"
                                    placeholder="Example: dev2alert"
                                    variant="outlined"
                                />
                                <Typography>/</Typography>
                                <Field
                                    component={TextField}
                                    name="repo"
                                    label="Repository"
                                    placeholder="Example: node-samp"
                                    variant="outlined"
                                />
                                <Tooltip title="Search">
                                    <IconButton type="submit">
                                        <SearchIcon />
                                    </IconButton>
                                </Tooltip>
                            </Box>
                        </Box>
                    </Box>
                </Paper>
            )}
        </Formik>
    );
};
