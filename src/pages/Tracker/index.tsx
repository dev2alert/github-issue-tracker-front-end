import React, {useMemo} from "react";
import {useLocation} from "react-router-dom";
import Container from "@mui/material/Container";
import {UseIssuesQueryOptions, useIssuesQuery} from "@common/api";
import {TrackerProvider} from "./Context";
import {TrackerIssueTable} from "./IssueTable";
import {TrackerPagination} from "./Pagination";
import {TrackerSearch} from "./Search";

export const TrackerPage: React.FC = () => {
    const {search} = useLocation();

    const issuesQueryOptions = useMemo<UseIssuesQueryOptions>(() => {
        const params = new URLSearchParams(search);
        const owner: string = params.get("owner") ?? "facebook";
        const repo: string = params.get("repo") ?? "react";
        const page = Number(params.get("page") ?? 1);
        const perPage = 30;

        return {owner, repo, page, perPage};
    }, [search]);
    const issuesQuery = useIssuesQuery(issuesQueryOptions);

    return (
        <TrackerProvider issuesQuery={issuesQuery} issuesQueryOptions={issuesQueryOptions}>
            <Container sx={{mt: 4}}>
                <TrackerSearch />
                <TrackerPagination />
                <TrackerIssueTable />
                <TrackerPagination />
            </Container>
        </TrackerProvider>
    );
};

export default TrackerPage;
