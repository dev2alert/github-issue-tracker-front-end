import React from "react";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Container from "@mui/material/Container";
import {useTheme} from "@mui/material/styles";

export const LoadingPage: React.FC = () => {
    const theme = useTheme();

    return (
        <Container>
            <Box
                sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "calc(100vh - " + theme.mixins.toolbar.minHeight + "px)"
                }}
            >
                <CircularProgress />
            </Box>
        </Container>
    );
};

export default LoadingPage;
