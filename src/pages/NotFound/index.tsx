import React from "react";
import {Navigate} from "react-router-dom";

export const NotFoundPage: React.FC = () => <Navigate to="/" />;

export default NotFoundPage;
