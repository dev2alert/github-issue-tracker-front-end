import React, {useCallback} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import Pagination from "@mui/material/Pagination";
import Paper from "@mui/material/Paper";
import {useStats} from "../Context";

export const StatsLogPagination: React.FC = () => {
    const navigate = useNavigate();
    const {search} = useLocation();
    const {
        logsQuery,
        logsQueryOptions: {page = 1}
    } = useStats();

    const count: number = logsQuery.isSuccess ? logsQuery.data.totalPages : 1;

    const handleChange = useCallback((event: React.ChangeEvent<unknown>, page: number) => {
        const params = new URLSearchParams(search);

        if (page !== 1) {
            params.set("page", page.toString());
        } else {
            params.delete("page");
        }

        navigate("/stats?" + params);
    }, []);

    if (!logsQuery.isSuccess || count <= 1) {
        return null;
    }

    return (
        <Paper sx={{display: "flex", justifyContent: "center", mt: 2, mb: 2, padding: 2}}>
            <Pagination page={page} count={count} color="primary" onChange={handleChange} />
        </Paper>
    );
};
