import React, {useContext} from "react";
import {UseLogsQueryOptions, UseLogsQueryResult} from "@common/api";

export interface StatsContextValue {
    logsQuery: UseLogsQueryResult;
    logsQueryOptions: UseLogsQueryOptions;
}

export const StatsContext = React.createContext<StatsContextValue | null>(null);

export const StatsProvider: React.FC<StatsContextValue & React.PropsWithChildren> = ({
    children,
    ...value
}) => {
    return <StatsContext.Provider value={value}>{children}</StatsContext.Provider>;
};

export function useStats(): StatsContextValue {
    const value = useContext(StatsContext);

    if (!value) {
        throw new Error("useStats must be used within a StatsProvider.");
    }

    return value;
}
