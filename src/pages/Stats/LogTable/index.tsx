import React, {useMemo} from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Skeleton from "@mui/material/Skeleton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import {useStats} from "../Context";

export const StatsLogTable: React.FC = () => {
    const {
        logsQuery,
        logsQueryOptions: {perPage}
    } = useStats();

    const loadingNode = useMemo<React.ReactNode>(
        () => (
            <TableBody>
                {[...Array(perPage ?? 10)].map((_, index) => (
                    <TableRow key={index}>
                        <TableCell width="25%">
                            <Skeleton />
                        </TableCell>
                        <TableCell width="25%">
                            <Skeleton />
                        </TableCell>
                        <TableCell width="25%">
                            <Skeleton />
                        </TableCell>
                        <TableCell width="25%">
                            <Skeleton />
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        ),
        []
    );

    if (logsQuery.isSuccess && logsQuery.data.items.length === 0) {
        return (
            <Box sx={{mt: 4.5, mb: 4}}>
                <Typography align="center">Log list empty</Typography>
            </Box>
        );
    }

    return (
        <TableContainer component={Paper} sx={{mt: 2}}>
            <Table sx={{minWidth: 650}}>
                <TableHead>
                    <TableRow>
                        <TableCell>№</TableCell>
                        <TableCell>Type</TableCell>
                        <TableCell>Time</TableCell>
                        <TableCell>User Ip</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {logsQuery.isSuccess && !logsQuery.isFetching
                        ? logsQuery.data.items.map(
                              (log) => (
                                  <TableRow key={log._id}>
                                      <TableCell width="25%">{log._id}</TableCell>
                                      <TableCell width="25%">{log.type}</TableCell>
                                      <TableCell width="25%">
                                          {new Date(log.time).toLocaleDateString(undefined, {
                                              year: "numeric",
                                              month: "short",
                                              day: "numeric",
                                              hour: "numeric",
                                              minute: "numeric",
                                              second: "numeric"
                                          })}
                                      </TableCell>
                                      <TableCell width="25%">{log.userIp}</TableCell>
                                  </TableRow>
                              ),
                              []
                          )
                        : null}
                </TableBody>
                {logsQuery.isFetching ? loadingNode : null}
            </Table>
        </TableContainer>
    );
};
