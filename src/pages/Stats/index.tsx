import React, {useMemo} from "react";
import {useLocation} from "react-router-dom";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import {UseLogsQueryOptions, useLogsQuery} from "@common/api";
import {StatsProvider} from "./Context";
import {StatsLogPagination} from "./LogPagination";
import {StatsLogTable} from "./LogTable";

export const StatsPage: React.FC = () => {
    const {search} = useLocation();

    const logsQueryOptions = useMemo<UseLogsQueryOptions>(() => {
        const params = new URLSearchParams(search);
        const page = Number(params.get("page") ?? 1);
        const perPage = 20;

        return {page, perPage};
    }, [search]);
    const logsQuery = useLogsQuery(logsQueryOptions);

    return (
        <StatsProvider logsQuery={logsQuery} logsQueryOptions={logsQueryOptions}>
            <Container sx={{mt: 4}}>
                <Typography variant="h5" component="h1">
                    Statistics
                </Typography>
                <StatsLogPagination />
                <StatsLogTable />
                <StatsLogPagination />
            </Container>
        </StatsProvider>
    );
};

export default StatsPage;
